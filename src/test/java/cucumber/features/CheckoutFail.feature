Feature: Checkout products
Background: user already login
  Scenario: user not fill checkout data
    Given user in cart page
    When user click checkout button
    Then in checkout step one
    When user input valid firstname
    And user input valid lastname
    And user not input zipcode
    And user click continue button
    Then error message appeared