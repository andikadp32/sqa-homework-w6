Feature: Logout

  Scenario: Logout Success
    Given user already login
    When user click sidebar menu button
    And user click logout button
    Then user logged out