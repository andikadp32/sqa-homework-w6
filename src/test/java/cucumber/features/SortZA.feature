Feature: Sorting Products
  Background: user already login
  Scenario: sorting products by name Z-A
    Given user is on products page
    When user click sort button
    And choose name ZA
    Then products sorted by name ZA